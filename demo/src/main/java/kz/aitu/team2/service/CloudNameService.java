package kz.aitu.team2.service;

import kz.aitu.team2.model.Name;
import kz.aitu.team2.repository.CloudNamesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CloudNameService {

    @Autowired
    private CloudNamesRepository cloudNamesRepository;

    public Name saveName(Name name) {
        return cloudNamesRepository.save(name);
    }

    public List<Name> saveNames(List<Name> name) {
        return cloudNamesRepository.saveAll(name);
    }

    public List<Name> getNames() {
        return cloudNamesRepository.findAll();
    }

    public Name getNameById(int id) {
        return cloudNamesRepository.findById(id).orElse(null);
    }


}
