package kz.aitu.team2.service;

import kz.aitu.team2.model.LocationDomain;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public interface LocationService {
    List<LocationDomain> findAll();

    public LocationDomain create(LocationDomain location);
}
