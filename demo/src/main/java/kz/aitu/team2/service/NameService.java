package kz.aitu.team2.service;

import kz.aitu.team2.model.Name;
import kz.aitu.team2.repository.CloudNamesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class NameService implements INameService {
    @Autowired
    private CloudNamesRepository cloudNamesRepository;

    @Override
    public List<Name> findAll() {
        var names = (List<Name>) cloudNamesRepository.findAll();
        return names;
    }
}
