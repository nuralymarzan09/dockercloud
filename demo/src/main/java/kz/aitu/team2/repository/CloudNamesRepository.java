package kz.aitu.team2.repository;

import kz.aitu.team2.model.Name;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CloudNamesRepository extends JpaRepository<Name, Integer> {
}
