package kz.aitu.team2.repository;

import kz.aitu.team2.model.LocationDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocatonRepo extends JpaRepository<LocationDomain, Integer> {
}
