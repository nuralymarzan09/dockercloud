package kz.aitu.team2.controller;

import kz.aitu.team2.model.Name;
import kz.aitu.team2.service.CloudNameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/list")
public class CloudNamesController {

    @Autowired
    private CloudNameService cloudNameService;


    @GetMapping("/read")
    public List<Name> findAllNames() {
        return cloudNameService.getNames();
    }

    @GetMapping("/read/{id}")
    public Name findByNameId(int id) {
        return cloudNameService.getNameById(id);
    }
}
