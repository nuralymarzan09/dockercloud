package kz.aitu.team2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;

@RestController
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    private ArrayList<String> names = new ArrayList<>();

    @GetMapping("/world")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        this.names.add(name);
        return String.format("Hello %s!", name);
    }

    @GetMapping("/list")
    public String listCollection() {
        Collections.sort(this.names);
        return String.format("Hello %s!", this.names.toString()
                .replace("[", "")
                .replace("]", "")
        );
    }
}
