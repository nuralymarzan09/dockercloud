package kz.aitu.team2.service;

import kz.aitu.team2.model.Name;
import kz.aitu.team2.repository.NameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class NameService implements INameService {
    @Autowired
    private NameRepository nameRepository;

    @Override
    public List<Name> findAll() {
        var names = (List<Name>) nameRepository.findAll();
        return names;
    }

    @Override
    public Name create(Name name) {
        return nameRepository.save(name);
    }
}
