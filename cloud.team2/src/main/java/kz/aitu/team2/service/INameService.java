package kz.aitu.team2.service;

import kz.aitu.team2.model.Name;

import java.util.List;

public interface INameService {
    List<Name> findAll();
    public Name create(Name name);
}
