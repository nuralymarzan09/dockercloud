package kz.aitu.team2.repository;

import kz.aitu.team2.model.Name;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NameRepository extends JpaRepository<Name, Long> {
    List<Name> findByName(String name);

    List<Name> findAll();
}
