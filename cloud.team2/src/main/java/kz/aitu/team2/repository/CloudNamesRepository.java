package kz.aitu.team2.repository;

import kz.aitu.team2.model.CloudName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CloudNamesRepository extends JpaRepository<CloudName, Integer> {
}
