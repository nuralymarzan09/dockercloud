package kz.aitu.team2.controller;

import kz.aitu.team2.model.Name;
import kz.aitu.team2.repository.NameRepository;
import kz.aitu.team2.service.INameService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/name")
public class NameController {
    @Autowired
    private NameRepository nameRepository;

    @Autowired
    private INameService iNameService;


    @GetMapping("/add")
    public String helloSecond(@RequestParam(value = "name") String name, Model model) {
        Name name1 = new Name();
        name1.setName(name);
        Name newName = iNameService.create(name1);
        model.addAttribute("newName", newName);
        System.out.println(name);
        return String.format("Hello %s!", name);
    }
}
